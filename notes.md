# Get before/after toolchains

```pseudo
for (sha, toolchain_prefix) in [(xxxxx, leaktest-prev), (xxxxx, leaktest)] {
    toolchain_name = "{toolchain_prefix}-{sha}"

    if !toolchain_exists(toolchain_name) {
        link_new_toolchain(sha, toolchain_name)
    }
}

fn link_new_toolchain(rust_git_sha, toolchain_name) {
    if !dir_exists(RUST_SRC_DIR) {
        clone_rust(RUST_SRC_DIR)
    }

    git_checkout(RUST_SRC_DIR, rust_git_sha)

    build_rust(RUST_SRC_DIR)

    output_target = "{BUILD_TARGET}/toolchins/{toolchain_name}"

    copy_dir("{RUST_SRC_DIR}/build/x86_64-unknown-linux-gnu/stage2", output_target)

    command!(rustup toolchain link (toolchain_name) (output_target))
}
```

# Build RLS with toolchains

```pseudo
if !dir_exists(RLS_SRC_DIR) {
    clone_rls(RLS_SRC_DIR)
}

build_rls(RLS_SRC_DIR, toolchain) # function already implemented
```