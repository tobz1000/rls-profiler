use command_macros::command;
use failure::{Error, Fail};
use log::info;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::time::Duration;

use crate::run_command::{run, run_with_timeout};

#[derive(Clone, Copy)]
pub struct Profiler<'a> {
    pub toolchain: &'a str,
    pub rls_dir: &'a str,
    pub test_proj_dir: &'a str,
    pub build_release: bool,
    pub timeout: Duration,
}

impl<'a> Profiler<'a> {
    pub fn build_rls(&self) -> Result<(), Error> {
        let Profiler { toolchain, rls_dir, build_release, .. } = *self;

        info!("Building {} mode", if build_release { "release" } else { "debug" });

        let mut build_cmd = command!(
            rustup run (toolchain)
            cargo build --target-dir=(rls_dir)/target/(toolchain)
            --no-default-features
            if build_release { --release }
        );
        build_cmd.current_dir(rls_dir);

        run(build_cmd)?;

        Ok(())
    }

    pub fn profile(&self) -> Result<(), Error> {
        let Profiler { toolchain, rls_dir, test_proj_dir, build_release, timeout, .. } = *self;

        info!(
            "Profiling {} mode; {:?} timeout",
            if build_release { "release" } else { "debug" },
            timeout
        );

        let toolchain_path = run(command!(rustup run (toolchain) rustc --print sysroot))?;

        let mut profile_cmd = command!(
            rustup run (toolchain)
            valgrind --tool=massif --massif-out-file=(rls_dir)/(toolchain).massif
            (rls_dir)/target/(toolchain)/(if build_release { "release" } else { "debug" })/rls
            --cli
        );
        profile_cmd.env("LD_LIBRARY_PATH", format!("{}/lib", toolchain_path.trim()));
        profile_cmd.current_dir(test_proj_dir);

        run_with_timeout(profile_cmd, timeout)?;

        Ok(())
    }

    pub fn get_peak_heap_bytes(&self) -> Result<u64, Error> {
        let Profiler { rls_dir, toolchain, .. } = *self;

        // Expects each snapshot in Massif output to have byte count line before the (if any) "is
        // peak heap" line
        info!("Reading profile output");

        const BYTES_LINE_START: &str = "mem_heap_B=";
        const HEAP_TREE_PEAK_LINE: &str = "heap_tree=peak";

        let out_file = File::open(format!("{}/{}.massif", rls_dir, toolchain))?;
        let reader = BufReader::new(out_file);

        let mut mem_heap_val: Option<u64> = None;

        for line in reader.lines() {
            let line = line?;

            if line.starts_with(BYTES_LINE_START) {
                mem_heap_val = Some(line[BYTES_LINE_START.len()..].parse()?);
            } else if line.trim() == HEAP_TREE_PEAK_LINE {
                if let Some(mem_heap_val) = mem_heap_val {
                    return Ok(mem_heap_val);
                } else {
                    // Found "is peak heap" specifier but never found a byte count
                    return Err(MaxHeapNotFound)?;
                }
            }
        }

        // Never found "is peak heap" specifier
        return Err(MaxHeapNotFound)?;
    }
}

#[derive(Debug, Fail)]
#[fail(display = "Maximum heap value not found in profile output")]
pub struct MaxHeapNotFound;