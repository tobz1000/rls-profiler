#![feature(proc_macro_hygiene)] // For command-macros::command

mod config;
mod profiler;
mod repo_state;
mod run_command;
mod toolchain;

use command_macros::command;
use failure::Error;
use log::{debug, error, info};
use std::time::Duration;

use crate::config::CONFIG;
use crate::profiler::Profiler;
use crate::repo_state::get_repo_state;
use crate::run_command::run;
use crate::toolchain::setup_toolchain;

fn main() {
    pretty_env_logger::formatted_timed_builder()
        .filter_level(CONFIG.log_level)
        .init();

    debug!("{:#?}", *CONFIG);

    match main_inner() {
        Ok(()) => {}
        Err(e) => error!("{}", e),
    }
}

fn main_inner() -> Result<(), Error> {
    let good_toolchain = &format!("before-rls-regr-{}", &CONFIG.good_toolchain_sha[..6]);
    let bad_toolchain = &format!("after-rls-regr-{}", &CONFIG.bad_toolchain_sha[..6]);

    info!("Using toolchains: {} {}", good_toolchain, bad_toolchain);

    setup_toolchain(
        &CONFIG.rust_src_dir,
        &CONFIG.good_toolchain_sha,
        &good_toolchain,
        &CONFIG.compatible_cargo_toolchain,
    )?;
    setup_toolchain(
        &CONFIG.rust_src_dir,
        &CONFIG.bad_toolchain_sha,
        &bad_toolchain,
        &CONFIG.compatible_cargo_toolchain,
    )?;

    run(command!(cargo clean --manifest-path=(CONFIG.test_proj_dir)/Cargo.toml))?;

    profile_for_toolchain(&good_toolchain)?;
    profile_for_toolchain(&bad_toolchain)?;

    Ok(())
}

fn profile_for_toolchain(toolchain: &str) -> Result<(), Error> {
    let rls_src_state = get_repo_state(&CONFIG.rls_dir)?;

    info!("Running for toolchain \"{}\"; {}", toolchain, rls_src_state);

    let profiler = Profiler {
        toolchain,
        rls_dir: &CONFIG.rls_dir,
        test_proj_dir: &CONFIG.test_proj_dir,
        build_release: CONFIG.build_rls_release,
        timeout: Duration::from_secs(CONFIG.profiler_run_timeout_secs)
    };

    profiler.build_rls()?;
    profiler.profile()?;
    let peak_heap_bytes = profiler.get_peak_heap_bytes()?;
    let peak_heap_mib = peak_heap_bytes as f64 / (1024 * 1024) as f64;

    info!("Max heap: {:.1}MiB", peak_heap_mib);

    Ok(())
}
