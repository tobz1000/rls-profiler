use command_macros::command;
use failure::Error;
use std::fmt;

use crate::run_command::{run, run_allowed_return_codes};

#[derive(Debug)]
pub struct RepoState {
    pub sha: String,
    pub branch: Option<String>,
    pub is_dirty: bool,
}

pub fn get_repo_state(repo_root_dir: &str) -> Result<RepoState, Error> {
    let sha = {
        let mut cmd = command!(git rev-parse HEAD);
        cmd.current_dir(repo_root_dir);
        run(cmd)?
    };

    let branch = {
        let mut cmd = command!(git symbolic-ref HEAD);
        cmd.current_dir(repo_root_dir);
        let (on_branch, stdout) = run_allowed_return_codes(
            cmd,
            |c| match c {
                Some(0) => Ok(true),
                Some(128) => Ok(false),
                _ => Err(())
            }
        )?;

        if on_branch {
            Some(stdout)
        } else {
            None
        }
    };

    let is_dirty = {
        let mut cmd = command!(git diff --quiet);
        cmd.current_dir(repo_root_dir);

        let (is_dirty, _stdout) = run_allowed_return_codes(
            cmd,
            |c| match c {
                Some(0) => Ok(false),
                Some(1) => Ok(true),
                _ => Err(())
            }
        )?;

        is_dirty
    };

    Ok(RepoState { sha, branch, is_dirty })
}

impl fmt::Display for RepoState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let RepoState { sha, branch, is_dirty } = self;

        if let Some(branch) = branch {
            write!(f, "{}:", branch.trim())?;
        }

        write!(f, "#{}", &sha.trim()[0..6])?;

        if *is_dirty {
            write!(f, " (dirty)")?;
        }

        Ok(())
    }
}