use command_macros::command;
use failure::{Error, Fail};
use fs_extra::dir;
use log::{debug, info};
use std::fs;

use crate::repo_state::get_repo_state;
use crate::run_command::{run, run_handle_output_lines};

pub fn setup_toolchain(
    rust_src_dir: &str,
    commit: &str,
    toolchain_name: &str,
    compatible_cargo_toolchain: &str
) -> Result<(), Error> {
    let installed_toolchains = run(command!(rustup toolchain list))?;
    let toolchain_is_present = installed_toolchains.lines().any(|t| t == toolchain_name);

    if toolchain_is_present {
        debug!("Toolchain {} already present", toolchain_name);

        return Ok(());
    }

    info!("Building toolchain {}", toolchain_name);

    let rust_src_state = get_repo_state(rust_src_dir)?;

    if rust_src_state.is_dirty {
        return Err(RustSrcDirDirty { dir: rust_src_dir.to_owned() })?;
    }

    // Checkout rust repo to specified commit
    {
        let mut checkout_cmd = command!(git checkout (commit));
        checkout_cmd.current_dir(rust_src_dir);
        run(checkout_cmd)?;
    }

    {
        let mut build_cmd = command!(./x.py build);
        build_cmd.current_dir(rust_src_dir);

        run_handle_output_lines(build_cmd, |line| {
            debug!("x.py: {}", line);
        })?;
    }

    info!("Copying Rust build output for toolchain {}", toolchain_name);

    let build_out_path = format!("{}/build/x86_64-unknown-linux-gnu/stage2", rust_src_dir);
    let rustup_home = run(command!(rustup show home))?;
    let toolchain_path = format!("{}/toolchains/{}", rustup_home.trim(), toolchain_name);

    let copy_options = dir::CopyOptions {
        copy_inside: true,
        ..dir::CopyOptions::new()
    };
    dir::copy(&build_out_path, &toolchain_path, &copy_options)?;

    copy_cargo(compatible_cargo_toolchain, toolchain_name)?;

    info!("Linking toolchain {}", toolchain_name);

    run(command!(rustup toolchain link (toolchain_name) (toolchain_path)))?;

    Ok(())
}

fn copy_cargo(from_toolchain: &str, to_toolchain: &str) -> Result<(), Error> {
    info!("Copying compatible `cargo` binary from toolchain {}", from_toolchain);

    run(command!(rustup toolchain install (from_toolchain)))?;
    run(command!(rustup component add cargo --toolchain (from_toolchain)))?;

    let from_base_path = toolchain_filepath(from_toolchain)?;
    let from_cargo_path = format!("{}/bin/cargo", from_base_path);

    let to_base_path = toolchain_filepath(to_toolchain)?;
    let to_cargo_path = format!("{}/bin/cargo", to_base_path);

    fs::copy(from_cargo_path, to_cargo_path)?;

    Ok(())
}

fn toolchain_filepath(toolchain: &str) -> Result<String, Error> {
    let toolchain_locations = run(command!(rustup toolchain list -v))?;

    for line in toolchain_locations.lines() {
        let split: Vec<&str> = line.split('\t').collect();

        if split.len() != 2 {
            return Err(ToolchainFilepathError::Parse { list_output: toolchain_locations })?;
        }

        if split[0] == toolchain {
            return Ok(split[1].to_owned());
        }
    }

    return Err(ToolchainFilepathError::NotInstalled { toolchain: toolchain.to_owned() })?;
}

#[derive(Debug, Fail)]
#[fail(display = "Rust source directory has uncommitted changes ({})", dir)]
pub struct RustSrcDirDirty {
    dir: String
}

#[derive(Debug, Fail)]
pub enum ToolchainFilepathError {
    #[fail(display = "Could not parse toolchain list:\n{}", list_output)]
    Parse { list_output: String },

    #[fail(display = "Toolchain {} not installed", toolchain)]
    NotInstalled { toolchain: String },
}