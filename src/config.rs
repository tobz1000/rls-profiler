//! Options configurable by env-variables, or a `.env` file.

use dotenv::dotenv;
use lazy_static::lazy_static;
use serde_derive::Deserialize;

lazy_static! {
    pub static ref CONFIG: Config = {
        let _ = dotenv();
        envy::from_env().unwrap()
    };
}

#[derive(Deserialize, Debug)]
#[serde(default = "default_config")]
pub struct Config {
    pub log_level: log::LevelFilter,

    // Filepath to RLS source directory
    pub rls_dir: String,

    // Filepath to Rust source
    pub rust_src_dir: String,

    // Filepath to test project on which to run RLS for profiling
    pub test_proj_dir: String,

    // Rust commit hash which doesn't exhibit regression
    pub good_toolchain_sha: String,

    // Name of toolchain with compatible `cargo` binary to link to toolchain-under-test
    pub compatible_cargo_toolchain: String,

    // Rust commit has which exhibits regression
    pub bad_toolchain_sha: String,

    // Whether to build RLS in release mode for profiling
    pub build_rls_release: bool,

    // How long to run profiling process before killing
    pub profiler_run_timeout_secs: u64
}

fn default_config() -> Config {
    Config {
        log_level: log::LevelFilter::Info,
        rls_dir: "/home/toby/sources/rls".to_owned(),
        rust_src_dir: "/home/toby/sources/rust".to_owned(),
        test_proj_dir: "/home/toby/sources/rls/test-proj".to_owned(),
        good_toolchain_sha: "757d6cc91a".to_owned(),
        bad_toolchain_sha: "b6e8f9dbdc".to_owned(), // "Remove the `alloc_jemalloc` crate"
        compatible_cargo_toolchain: "nightly-2018-11-03-x86_64-unknown-linux-gnu".to_owned(),
        build_rls_release: true,
        profiler_run_timeout_secs: 15,
    }
}
