use failure::Fail;
use log::debug;
use os_pipe::pipe;
use std::fmt;
use std::io::{BufReader, BufRead};
use std::process::{Command, Output, Stdio};
use std::time::Duration;
use std::thread::sleep;

#[derive(Debug, Fail)]
pub struct CommandFail {
    command: String,
    cause: FailCause,
}

#[derive(Debug)]
enum FailCause {
    RunFail {
        err_code: Option<i32>,
        err_out: String,
    },
    ProcFail {
        err: std::io::Error,
    },
}

impl fmt::Display for CommandFail {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let CommandFail { command, cause } = self;

        write!(f, "Command failed: {}", command)?;

        match cause {
            FailCause::RunFail {
                err_code,
                err_out,
            } => {
                if let Some(err_code) = err_code {
                    write!(f, "\n\tCode: {}", err_code)?;
                }

                if !err_out.is_empty() {
                    write!(f, "\n\tOutput: {}", err_out)?;
                }
            }
            FailCause::ProcFail { err } => {
                write!(f, "\n\tProcess failure: {}", err)?;
            }
        }

        Ok(())
    }
}

pub fn run(command: Command) -> Result<String, CommandFail> {
    let ((), stdout) = run_allowed_return_codes(command, |c| match c {
        Some(0) => Ok(()),
        _ => Err(())
    })?;

    Ok(stdout)
}

pub fn run_allowed_return_codes<T>(
    mut command: Command,
    is_success_code: impl Fn(Option<i32>) -> Result<T, ()>
) -> Result<(T, String), CommandFail> {
    debug!("run {:?}", &command);

    let output = map_io_err(command.output(), &command)?;

    map_output(output, &command, is_success_code)
}

/// Returns `Ok(Some(_))` if the command finishes before the timeout; `Ok(None)` if the command is
/// running up to the timeout and killed; `Err(_)` otherwise.
pub fn run_with_timeout(
    mut command: Command,
    timeout: Duration
) -> Result<Option<String>, CommandFail> {
    debug!("run_with_timeout={}ms {:?}", timeout.as_millis(), &command);

    command.stdout(Stdio::piped());
    command.stderr(Stdio::piped());

    // TODO: capture stdout/stderr
    let mut child = map_io_err(command.spawn(), &command)?;

    sleep(timeout);

    let proc_has_finished = map_io_err(child.try_wait(), &command)?.is_some();

    if proc_has_finished {
        let output = map_io_err(child.wait_with_output(), &command)?;

        let ((), stdout) = map_output(output, &command, |c| match c {
            Some(0) | None => Ok(()),
            _ => Err(())
        })?;

        Ok(Some(stdout))
    } else {
        map_io_err(child.kill(), &command)?;

        Ok(None)
    }
}

// Combines stdout and stderr, and performs an action on each output line as they come
pub fn run_handle_output_lines(
    mut command: Command,
    handle_line: impl Fn(String),
) -> Result<(), CommandFail> {
    // Grab command string for error reporting before dropping command
    let command_desc = format!("{:?}", command);

    debug!("run_handle_output_lines {}", command_desc);

    let (reader, writer) = map_io_err(pipe(), &command_desc)?;
    let writer_clone = map_io_err(writer.try_clone(), &command_desc)?;

    // Combine stdout and stderr from build process
    command.stdout(writer_clone).stderr(writer);

    let mut child = map_io_err(command.spawn(), &command_desc)?;

    // File descriptors passed to `command` are themselves duplicated on `.spawn()`, so our initial
    // descriptors must be dropped, so that the pipe is not held open when the child process is
    // finished.
    // Ref: https://docs.rs/os_pipe/0.9.1/os_pipe/#example
    drop(command);

    let buf_reader = BufReader::new(reader);

    for line in buf_reader.lines() {
        let line = map_io_err(line, &command_desc)?;

        handle_line(line);
    }

    let exit_status = map_io_err(child.wait(), &command_desc)?;

    if exit_status.success() {
        Ok(())
    } else {
        Err(CommandFail {
            command: command_desc,
            cause: FailCause::RunFail {
                err_code: exit_status.code(),
                err_out: "".to_owned(),
            }
        })
    }
}

fn map_io_err<T>(
    result: Result<T, std::io::Error>,
    command_desc: impl fmt::Debug,
) -> Result<T, CommandFail> {
    match result {
        Ok(t) => Ok(t),
        Err(err) => Err(CommandFail {
            command: format!("{:?}", command_desc),
            cause: FailCause::ProcFail { err }
        })
    }
}

fn map_output<T>(
    Output { status, stdout, stderr }: Output,
    command_desc: impl fmt::Debug,
    map_code: impl Fn(Option<i32>) -> Result<T, ()>
) -> Result<(T, String), CommandFail> {
    if let Ok(val) = map_code(status.code()) {
        let stdout_string = String::from_utf8_lossy(&stdout).into_owned();

        return Ok((val, stdout_string));
    }

    let err_out = String::from_utf8_lossy(&stderr).into_owned();

    Err(CommandFail {
        command: format!("{:?}", command_desc),
        cause: FailCause::RunFail {
            err_code: status.code(),
            err_out,
        },
    })
}
